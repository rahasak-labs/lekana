name := "lekana"

version := "1.0"

scalaVersion := "2.11.7"

libraryDependencies ++= {

  Seq(
    "com.datastax.oss"          % "java-driver-core"        % "4.4.0",
    "org.slf4j"                 % "slf4j-log4j12"           % "1.7.26",
    "log4j"                     % "log4j"                   % "1.2.16",
    "org.conscrypt"             % "conscrypt-openjdk"       % "2.2.1",
    "com.typesafe"              % "config"                  % "1.4.0",
    "org.scalatest"             % "scalatest_2.11"          % "2.2.1"               % "test"
  )
}

resolvers ++= Seq(
  "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)

assemblyMergeStrategy in assembly := {
  case PathList(ps @ _*) if ps.last endsWith ".properties" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "module-info.class" => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "pom.xml" => MergeStrategy.first
  case x if x.contains("asm") => MergeStrategy.first
  case "module-info.class" => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

