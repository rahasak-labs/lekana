package com.rahasak.lekana

import java.net.InetSocketAddress

import com.datastax.oss.driver.api.core.CqlSession

trait CassandraCluster extends CassandraConf {
  val session =
    CqlSession.builder()
      .addContactPoint(new InetSocketAddress(cassandraHosts.head, 9042))
      .withLocalDatacenter("DC1")
      .build()
}



