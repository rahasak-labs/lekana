package com.rahasak.lekana

import com.datastax.oss.driver.internal.core.os.Native

object Main extends CassandraCluster {
  def main(args: Array[String]): Unit = {
    // does not currently support runtime class generation via something like ASM so we
    // explicitly fall back to jnr.ffi.provider.jffi.ReflectionLibraryLoader here
    System.setProperty("jnr.ffi.asm.enabled", "false")

    val result = session.execute("select keyspace_name from system_schema.keyspaces").all()
    println(result)
    println(result.size())
  }
}
