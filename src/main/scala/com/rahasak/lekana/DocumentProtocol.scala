package com.rahasak.lekana

import java.util.{Date, UUID}

case class Signature(signer: String, comment: String, timestamp: Long)

case class Document(id: UUID, company: String, name: String, tags: List[String], signatures: List[Signature],
                    status: String, timestamp: Date = new Date)

