FROM oracle/graalvm-ce:20.1.0

MAINTAINER Eranga Bandara (erangaeb@gmail.com)

# install native-image
RUN gu install native-image

# work dir on /app
WORKDIR /app

# copy file
ADD proxy.json .
ADD target/scala-2.11/lekana-assembly-1.0.jar lekana.jar

# command
RUN native-image \
 -H:+ReportExceptionStackTraces \
 -H:+TraceClassInitialization \
 -H:IncludeResources=.*Driver\\.properties \
 -H:IncludeResources=.*reference\\.conf \
 -H:IncludeResources=jni/.* \
 -H:DynamicProxyConfigurationFiles=proxy.json \
 -H:+JNI \
 --initialize-at-build-time=jnr.ffi.provider.LoadedLibrary \
 --initialize-at-build-time=jnr.ffi.Pointer \
 --initialize-at-build-time=jnr.ffi.Struct \
 #-H:DynamicProxyConfigurationFiles="dynamic-proxy-config.json" \
 -Dio.netty.noUnsafe=true \
 --static \
 --verbose \
 --no-fallback \
 --allow-incomplete-classpath \
 --report-unsupported-elements-at-runtime \
 -jar lekana.jar lekana

# command
ENTRYPOINT ["/app/lekana"]


